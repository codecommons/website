![Code Commons logo](img/logo.png)

- New community of practice for people in Tucson working with code and building software
- Bring your computer, your projects, and your ideas
- Starting 2025 January 22, we meet **Wednesdays 2-6 PM** at UA Main Library ([map](https://www.openstreetmap.org/?mlat=32.23103&mlon=-110.94897#map=19/32.23103/-110.94897)).
  - Look around [CATalyst Studios](https://lib.arizona.edu/catalyst) for the tabletop sign with our logo.
  - **We may be in a different room than you expect**, as the Data Studio is no longer ours to use every Wednesday.
- Subscribe to our low-traffic [mailing list](https://groups.io/g/code-commons) for occasional updates, and ad-hoc planning for summer meetups

Code Commons provides a physical space for community and collaboration. Join us to share experience, learn, mentor, discover opportunities, and work on your programming projects in the presence of others doing the same.

Code Commons also aims to provide the best aspects of workplace culture for people who are otherwise isolated from other developers. This includes (but isn't limited to) people working on free, libre, open-source software (FLOSS), and software for public service missions.

## Is Code Commons for me?

Probably! Code Commons welcomes you whether you’re learning programming for the first time, or you have worked with code for decades. Whether or not people would stereotype you as a student, a hacker, or a software engineer. Whether or not you're part of the university.

Maybe your work is mostly research with incidental programming. Maybe you’re in a less-technical role like project management. Maybe you make mostly-physical, incidentally-digital things. Maybe you use a computer in creative ways beyond programming, like writing, design, and making art. Maybe you’re a civic hacker, using public data to inform policy and governance. Maybe you build proprietary software, but you are open-minded about FLOSS. Code Commons welcomes all of these people, and more.

The only people who are not welcome are:

- People who are there mostly to sell a product or service to other attendees
- Recruiters seeking attendees' personal information to sell access to them
- People who would not abide by the following code of conduct

## Code of Conduct

Code Commons is a professional and inclusive gathering that does not tolerate harassment or threatening behavior. Community members must treat each other with the respect and excellence that a modern workplace would expect. People attending Code Commons must also abide by the [University Libraries code of conduct](https://new.library.arizona.edu/policies/code-of-conduct).

## Can you help with my programming project?

Maybe! Code Commons isn't exactly a tutoring or consulting service. We can't promise that someone will be ready to help when you show up. But if you're stuck on a problem, someone might be able to help, or point you in the right direction. We also know a lot of other developers, and we might be able to point you to someone more helpful.

If you come asking for help, please be patient and respectful of other people's attention. They may also be struggling with something. And if you can, stick around to help others too.

## How will Code Commons evolve?

Code Commons might grow into a more permanent space where people can gather on any day at any time. It could also grow to another location in Tucson. If you're not in Tucson, we specifically encourage people to start new chapters of Code Commons in other cities! Please see [this blog post](https://cmart.blog/code-commons) for more about the background and vision for Code Commons.

## What are some other local groups? How is Code Commons different from them?

Code Commons is not a maker space per se, but [it is hosted at one](https://libguides.library.arizona.edu/catalyst), and you could say Code Commons is the software-oriented aspect of a maker space.

[Research Bazaar](https://researchbazaar.arizona.edu) is focused on data science. Some people who work with code are data scientists, but many others are not.

[Data & Viz Drop-in](https://libcal.library.arizona.edu/event/9325842) is a weekly event for helping with data science projects. Code Commons is broader than data science and is focused more on building community than helping visitors.

[TucsonJS](https://github.com/TucsonJS) focuses on web development and JavaScript. TucsonJS meets monthly for an evening of snacks, conversation, and possibly a presentation.

[Tucson Python Meetup](https://www.meetup.com/Tucson-Python-Meetup) offers approximately monthly events for people working with the Python programming language. Code Commons is a more frequent and less-structured gathering. We do write Python but we aren't limited to it.

[Tucson Functional Programmers](https://www.meetup.com/Tucson-Functional-Programmers) offers approximately monthly events for people working with [functional](https://en.wikipedia.org/wiki/Functional_programming) programming techniques and languages. Code Commons is more frequent, and we love functional code but are not limited to it.

[UX@UA](https://uxua.arizona.edu) is a learning community of user experience practitioners at University of Arizona. UX@UA hosts networking events and lectures, approximately monthly during academic semesters. Code Commons is more frequent and less structured. Our focus is more on software and programming, but these activities tend to have strong overlap with UX.

Finally, Code Commons is not a hackathon. Hackathons tend to be intensive one-time events with a goal of launching a new project quickly, while Code Commons is more about the everyday practice.

## Who is behind Code Commons?

Right now, Code Commons is organized by:

- Chris Martin: email at cmart dot today
- Devin Bayly: baylyd at arizona dot edu

We're building Code Commons with guidance from local colleagues and friends including David LeBauer, Jen Nichols, Jeff Oliver, Julian Pistorius, and Mariah Wall.

## Other

[Groove Basin link](https://c-mart.sandcats.io/shared/DkW3kVyqFuRg3cUO8wD_Z2AQdr12pAFpN1wpFkNZ-Ut) for coding music. (This is a listen-only link; ask an organizer for access to control the stream and add music.)
