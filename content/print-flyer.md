![logo](img/logo.png)

**New community of practice for people in Tucson who work with code and build software**

- Bring your computer, your projects, your ideas
- Work in friendly, sociable company
- Learn, mentor, and discover opportunities

We welcome students, non-traditional learners, software engineers, web developers, makers, data scientists, civic hackers, remote workers, and more! It's a big tent.

We meet **Wednesdays 2-6 PM** at **CATalyst Data Studio, University of Arizona Main Library**.

(on your left as you enter the library)

Learn more at **codecommons.net**